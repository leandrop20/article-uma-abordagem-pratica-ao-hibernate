package br.com.devmedia.myforum.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;

import org.hibernate.Session;
import org.hibernate.Transaction;

import br.com.devmedia.myforum.persistence.HibernateUtil;

@WebFilter(urlPatterns="/*")
public class OpenSesionInViewFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse respose, FilterChain chain)
			throws IOException, ServletException {
		Session session = HibernateUtil.getSession();
		Transaction tx = session.getTransaction();
		tx.begin();
		
		try {
			chain.doFilter(request, respose);
			tx.commit();
		} catch (Throwable e) {
			tx.rollback();
			throw e;
		} finally {
			if (session.isOpen()) session.close();
		}
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		
	}

	@Override
	public void destroy() {

	}

}