package br.com.devmedia.myforum.persistence;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

import br.com.devmedia.myforum.modelo.Assunto;
import br.com.devmedia.myforum.modelo.Usuario;

public class HibernateUtil {

	private static SessionFactory sessionFactory;
	
	static {
		Configuration configuration = new Configuration();
		configuration.addAnnotatedClass(Usuario.class);
		configuration.addAnnotatedClass(Assunto.class);
		configuration.configure();
		
		ServiceRegistryBuilder serviceRegistryBuilder = new ServiceRegistryBuilder()
				.applySettings(configuration.getProperties());
		ServiceRegistry serviceRegistry = serviceRegistryBuilder.buildServiceRegistry();
		sessionFactory = configuration.buildSessionFactory(serviceRegistry);
	}
	
	public static Session getSession() {
		Session session = sessionFactory.getCurrentSession();
		return session;
	}
	
}