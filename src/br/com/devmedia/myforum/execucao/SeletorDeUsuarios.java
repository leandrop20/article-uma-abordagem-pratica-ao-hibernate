package br.com.devmedia.myforum.execucao;

import org.hibernate.LockOptions;
import org.hibernate.Session;

import br.com.devmedia.myforum.modelo.Usuario;
import br.com.devmedia.myforum.persistence.HibernateUtil;

public class SeletorDeUsuarios {

	public static void main(String[] args) {
		Session session = HibernateUtil.getSession();
		
		Usuario usuario = null;
		
		//Obt�m diretamente um usu�rio com o Id = 1
		usuario = (Usuario) session.byId(Usuario.class).load(1L);
		
		//Obt�m apenas uma refer�ncia ao usu�rio com id = 1 
		//a possuir seus dados carregados apenas quando necess�rio (Lazy Loading)
		// - similar ao antigo m�todo Session.get()
		usuario = (Usuario) session.byId(Usuario.class).getReference(1L);
		
		//Obtendo um objeto apenas com lock de leitura
		usuario = (Usuario) session.byId(Usuario.class).with(LockOptions.READ).load(1L);
		
		//Obtendo um usu�rio por e-mail (�nico @NaturalId definido na classe Usuario)
		usuario = (Usuario) session.bySimpleNaturalId(Usuario.class).getReference("renandemelo@gmail.com");
		
		//Mesmo cen�rio acima com lock de leitura
		usuario = (Usuario) session.bySimpleNaturalId(Usuario.class).with(LockOptions.READ)
				.load("renandemelo@gmail.com");
		//Especificando qual atributo anotado com @NaturalId deve ser utilizado na consulta 
		usuario = (Usuario) session.byNaturalId(Usuario.class).using("email", "renandemelo@gmail.com").load();
		
		//Mesmo cen�rio acima com lock de leitura
		usuario = (Usuario) session.byNaturalId(Usuario.class).with(LockOptions.READ)
				.using("email", "renandemelo@gmail.com").load();
		
		System.out.println("Usu�rio:" + usuario.getNome());
		session.close();
	}
	
}