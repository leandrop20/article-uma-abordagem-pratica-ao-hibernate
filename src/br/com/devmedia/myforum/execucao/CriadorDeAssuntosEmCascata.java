package br.com.devmedia.myforum.execucao;

import org.hibernate.Session;

import br.com.devmedia.myforum.modelo.Usuario;
import br.com.devmedia.myforum.persistence.HibernateUtil;

public class CriadorDeAssuntosEmCascata {

	public static void main(String[] args) {
		Session session = HibernateUtil.getSession();
		session.getTransaction().begin();
		
		Usuario usuario = (Usuario) session.byId(Usuario.class).load(1L);
		usuario.criaAssunto("Frameworks");
		session.getTransaction().commit();
		session.close();
	}
	
}