package br.com.devmedia.myforum.execucao;

import org.hibernate.Session;

import br.com.devmedia.myforum.modelo.Usuario;
import br.com.devmedia.myforum.persistence.HibernateUtil;

public class ExcluidorDeUsuarios {

	public static void main(String[] args) {
		Session session = HibernateUtil.getSession();
		session.getTransaction().begin();
		
		Usuario usuario = (Usuario) session.byId(Usuario.class).load(1L);
		session.delete(usuario);
		session.getTransaction().commit();
		session.close();
	}
	
}