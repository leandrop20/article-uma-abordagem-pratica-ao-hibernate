package br.com.devmedia.myforum.execucao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import br.com.devmedia.myforum.modelo.Usuario;
import br.com.devmedia.myforum.persistence.HibernateUtil;

public class SeletorDeUsuariosPorQuery {

	public static void main(String[] args) {
		Session session = HibernateUtil.getSession();
		Query query = null;
		List<Usuario> usuarios = null;
		
		//selecionando todos os usu�rios da base
		usuarios = session.createQuery("from Usuario").list();
		
		//todos os usu�rios com nome = 'Renan'
		query = session.createQuery("from Usuario where nome = :nome");
		query.setParameter("nome", "Renan");
		usuarios = query.list();
		
		//todos os usu�rios com email LIKE '%gmail%'
		query = session.createQuery("from Usuario where email like :email");
		query.setParameter("email", "%gmail%");
		usuarios = query.list();
		
		//todos os usu�rios com email LIKE '%gmail%' e nome = 'Renan'
		query = session.createQuery("from Usuario where email like :email and nome = :nome");
		query.setParameter("nome", "Renan");
		query.setParameter("email", "%gmail%");
		usuarios = query.list();
		
		for (Usuario usuario : usuarios) {
			System.out.println("Usu�rio: " + usuario.getNome());
		}
		
		session.close();
	}
}