package br.com.devmedia.myforum.execucao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import br.com.devmedia.myforum.modelo.Usuario;
import br.com.devmedia.myforum.persistence.HibernateUtil;

public class SeletorDeUsuariosPorCriteria {

	public static void main(String[] args) {
		Session session = HibernateUtil.getSession();
		session.beginTransaction();
		
		Criteria criteria = null;
		List<Usuario> usuarios = null;
		
		usuarios = session.createCriteria(Usuario.class).list();
		
		criteria = session.createCriteria(Usuario.class);
		criteria.add(Restrictions.eq("nome", "Renan"));
		usuarios = criteria.list();
		
		criteria = session.createCriteria(Usuario.class);
		criteria.add(Restrictions.like("email", "gmail", MatchMode.ANYWHERE));
		usuarios = criteria.list();
		
		criteria = session.createCriteria(Usuario.class);
		criteria.add(Restrictions.eq("nome", "Renan"));
		criteria.add(Restrictions.like("email", "gmail", MatchMode.ANYWHERE));
		usuarios = criteria.list();
		
		for (Usuario usuario : usuarios) {
			System.out.println("Usu�rio: " + usuario.getNome());
		}
		
		session.close();
	}
	
}