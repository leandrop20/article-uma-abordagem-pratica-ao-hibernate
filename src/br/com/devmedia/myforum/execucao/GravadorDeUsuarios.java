package br.com.devmedia.myforum.execucao;

import org.hibernate.Session;
import org.hibernate.Transaction;

import br.com.devmedia.myforum.modelo.Usuario;
import br.com.devmedia.myforum.persistence.HibernateUtil;

public class GravadorDeUsuarios {

	public static void main(String[] args) {
		Usuario usuario = new Usuario("Renan", "renandemelo@gmail.com", "abcdef");
		Session session = HibernateUtil.getSession();
		Transaction tran = session.beginTransaction();
		session.persist(usuario);
		tran.commit();
		session.close();
	}
	
}