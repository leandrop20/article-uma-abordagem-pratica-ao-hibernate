package br.com.devmedia.myforum.execucao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import br.com.devmedia.myforum.modelo.Usuario;
import br.com.devmedia.myforum.persistence.HibernateUtil;

public class RelatorioDeAutores {

	public static void main(String[] args) {
		Session session = HibernateUtil.getSession();
		session.beginTransaction();
		
		Query query = session.createQuery("from Usuario");
		List<Usuario> usuarios = query.list();
		
		for (Usuario usuario : usuarios) {
			if (usuario.isAutor()) {
				System.out.println("Autor: " + usuario.getNome());
			}
		}
		
		session.close();
	}
	
}