package br.com.devmedia.myforum.execucao;

import org.hibernate.Session;

import br.com.devmedia.myforum.modelo.Assunto;
import br.com.devmedia.myforum.modelo.Usuario;
import br.com.devmedia.myforum.persistence.HibernateUtil;

public class CriadorDeAssuntos {

	public static void main(String[] args) {
		Session session = HibernateUtil.getSession();
		session.getTransaction().begin();
		
		Usuario usuario = (Usuario) session.byId(Usuario.class).load(1L);
		Assunto assunto = new Assunto("JSF2");
		session.persist(assunto);
		usuario.getCriados().add(assunto);
		session.getTransaction().commit();
		session.close();
	}
	
}